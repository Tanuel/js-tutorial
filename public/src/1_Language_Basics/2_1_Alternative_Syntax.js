//##########################
//you can use shorter syntax for some operations

let myVar = 10;

//add +10 to the variable:

myVar = myVar + 10; //long notation
myVar += 10; //does the exact same thing;

//works with a lot more operators

myVar -= 5;
myVar *= 7;
myVar /= 5;
myVar %= 3;


//##########################
//Increment - add +1 to a variable;

myVar = 0;

myVar++; // myVar => 1 (basically this is myVar +=1)
myVar++; // myVar => 2


++myVar; // myVar => 3
++myVar; // myVar => 4


//Difference between myVar++ and ++myVar?

myVar = 0;
console.log(myVar++); //0 - return value, THEN increment it
console.log(myVar++); //1 - return value, THEN increment it
console.log(myVar); //2

myVar = 0;
console.log(++myVar); //1 - increment value, THEN return it
console.log(++myVar); //2 - increment value, THEN return it
console.log(myVar); //2


//##########################
//Decrement - subtract 1 from a variable
myVar = 0;
myVar--; // myVar => -1
myVar--; // myVar => -2


--myVar; // myVar => -2
--myVar; // myVar => -2


//##########################
//More examples

let a = 10;

let b = a++; //a = 11, b = 10;

a = 10;
b = ++a; //a = 11, b = 11;

