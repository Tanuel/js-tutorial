//Arrays

//An array has multiple values

let myArray = [1, 2, 3];

console.log(myArray); // [1, 2, 3]

//accessing values (index starts at 0!)

console.log(myArray[0]); //1
console.log(myArray[1]); //2
console.log(myArray[2]); //3

console.log(myArray[3]); //undefined

//How many values are in the array?
// => use the length property
console.log(myArray.length);

//access array values with a dynamic index

let index = 0;

console.log(myArray[index]);   // = 1
console.log(myArray[++index]); // = 2
