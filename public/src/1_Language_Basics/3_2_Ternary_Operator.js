//##########################
//Ternary Operator
//##########################

let a = 5,
    b = 10;

//there is a short way to make simple comparisons
let result;
if (a === b) {
    result = "equals";
} else {
    result = "not equals";
}

//This can be written in a single line using the ternary operator

result = a === b ? "equals" : "not equals";

/* Schema:
    comparison ? return this if it is true : return this if it is false
 */

//Example: find the higher number
let higherNumber = a > b ? a : b;

console.log("The higher number is " + higherNumber);