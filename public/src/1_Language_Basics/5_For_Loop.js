//Looping over an array with a for loop

let myArray = [1, 2, 3];

for (let i = 0; i < myArray.length; i++) {
    console.log(myArray[i]);
}

//breaking out of an array  - finding a certain entry

for (let i = 0; i < myArray.length; i++) {
    if (myArray[i] === 2) {
        console.log("I found the entry with value 2 - the index is " + i);
        break;
    }

}

//skipping an entry
for (let i = 0; i < myArray.length; i++) {
    if (myArray[i] === 2) {
        //if the comparison is true, skip the rest of this iteration and go to the next
        continue;
    }
    console.log(myArray[i]);
}