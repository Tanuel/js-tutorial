//##########################
//IF-Statements
//##########################

//We want a certain thing to happen if a certain statement is true

let a = 5;
let b = 10;
if (a === 5) {
    console.log("a equals 5");
}

if (a === b) {
    console.log("This does not get printed");
}

if (a * 2 === b) {
    console.log('b is 2*a');
}

if (true) {
    console.log("this gets printed always!");
}
if (false) {
    console.log("this never gets printed!");
}

//##########################
//The ELSE keyword
//##########################

//We want a certain thing to happen, if the statement is not true
if (a === b) {
    console.log("a equals b");
} else {
    console.log("a does not equal b");
}

//chaining
if (a === b) {
    console.log("a equals b");
} else if (a * 2 === b) {
    console.log("2*a equals b");
} else {
    console.log("a does not equal b, neither does 2*a")
}
