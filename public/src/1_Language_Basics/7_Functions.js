//##########################
// Functions
//##########################

//Functions allow you to reuse often used operations

function sayHello() {
    console.log("Hello World!");
}

sayHello(); //Hello World!
sayHello(); //Hello World!

//Pass a parameter to do something for you
function sayMyName(myName) {
    console.log("Hello there, " + myName);
}

sayMyName('Tanuel');     //Hello there, Tanuel
sayMyName('JavaScript'); //Hello there, JavaScript

let name = 'my name in a variable';
sayMyName(name); //Hello there, my name in a variable

//##########################
//Use a function to do operations for you (return a result)
function add(a, b) {
    return a + b;
}

let result = add(1, 2);
console.log(result); //3

result = add(2, 5);
console.log(result); //7

//Use case: find a value
function findValue(arr, x) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === x) {
            //we found x in the array, return the index and exit the function
            return i;
        }
    }
    //we did not find x in the array, return -1
    return -1;
}

let myArray = [4, 656, 23446, 24, 7, 3, 6, 8767, 32, 3456, 46, 245, 13456];

result = findValue(myArray, 3); //returns 5
if (result >= 0) {
    console.log("The value was found at index " + result);
} else {
    console.log("The value was not found in the array");
}