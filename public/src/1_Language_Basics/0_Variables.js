//##########################
// Variables
//##########################
// Declare empty variable

var myVar;    //function scope
let myLetVar; //block scope

// Assign values

myVar = "my value";
myLetVar = "my other value";

//or combine it:

var a = "a";
let b = "b";

// constants (these values can NOT be reassigned)
const myConst = "this value can not be changed2";

//Show variable contents

//standard log
console.log(a);

//show as a warning
console.warn(a);

//show as an error
console.error("an error occured", a);

//This will throw an error, because a constant can not be changed:
myConst = "error?";

//multiple variable declaration:
let v1, v2, v3 = "v3";

//or multiline:
let v4,
    v5 = "v5",
    v6;

//Scope difference between let/const and var

// let and const are only available within a {}-block

{
    let v7 = "v7";
    const v8 = "v8";
}
console.log(v7); //error: v7 is not defined
console.log(v8); //error: v8 is not defined

{
    var v9 = "v9";
}
console.log(v9); //works

//Pro-Tip: Always use let or const, avoid using var!