//Do something until a certain condition is true

let x = 1;

//repeat this block until x is greater than 100
while (x < 100) {
    x *= 2;
    console.log(x);
}

//endless loop (escape with "break;")
while (true) {
    console.log("endless loop..... better break now");
    break;
}

//if the condition is false to begin with, the loop does not get executed
while (false) {
    console.log("this statement is unreachable")
}

let myArray = [2, 5, 9, 8, 3, 4, 5, 9, 6, 6, 3, 4, 6, 4];
let i = 0;
//skip a loop with continue;
while (i < myArray.length) {
    if (myArray[i++] !== 3) {
        console.log("This entry is not 3...");
        continue;
    }
    console.log("i found 3 at index " + i);
}