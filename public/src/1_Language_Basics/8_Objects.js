//##########################
// Objects
//##########################
//Objects are a bundle of variables and functions

//Initialize an empty object
let obj1 = {};

//Init object with properties
let obj2 = {
    key: "value",
    key2: "value2"
};
//can be done in oneline, but looks uglier
let obj3 = {key: "value", key2: "value2"};


//Accessing properties
console.log(obj3.key);  //"value"
console.log(obj3.key2); //"value2"

//changing properties
obj3.key = "i changed this value!";
console.log(obj3.key); //"i changed this value"


//Accessing properties dynamically
console.log(obj3["key2"]); //"value2"

let myKey = "key2";
console.log(obj3[myKey]); //"value2"

//##########################
//Functions in an object
//##########################

let MathObj = {
    add: function (a, b) {
        return a + b;
    },
    multiply: function (a, b) {
        return a * b;
    }
};

let x = MathObj.add(5, 10); //x = 15
let y = MathObj.multiply(5, 10); //x = 50

console.log("5 + 10 = " + x);
console.log("5 * 10 = " + y);

//##########################
//Nested Objects
//##########################

//you can put an object in an object
let obj4 = {
    nest: {
        value: "i am a nested value",
        anotherNest: {
            birdName: "Twitter"
        }
    }
};

console.log(obj4.nest.value); //"i am a nested value"
console.log(obj4.nest.anotherNest.birdName); //"Twitter"
