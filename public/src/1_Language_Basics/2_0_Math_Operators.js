const a = 1;
const b = 2;
const c = 3;

//##########################
// Addition
//##########################

console.log(a + b);     // 3 = 1 + 2
console.log(a + b + c); // 6 = 1 + 2 + 3

let add1 = a + b;
console.log(add1); // 3;

add1 = add1 + 1;
console.log(add1); // 4


//##########################
//Subtract

console.log(a - b);      // -1 =  1 - 2
console.log(-a - b - c); // -6 = -1 - 2 - 3

let sub1 = a - b;
console.log(sub1); // -1;

sub1 = sub1 - 1;
console.log(sub1); // -2;

//##########################
//multiplication

console.log(a * b); //2 = 1 * 2
console.log(b * c); //6 = 2 * 3

//##########################
//division
console.log(a / b); //1 / 2 = 0.5
console.log(b / c); //2 / 3 = 0.6666666666666666

console.log(1 / 0); // Infinity
console.log(0 / 1); // 0

//##########################
//Operator precedence - point before line

// * and / have precedence over + and -, just like you learn in school
console.log(1 + 2 * 4); //9
console.log(1 - 2 / 4); //0.5

//use brackets to override precedence:
console.log((1 + 2) * 4); // 12
console.log((1 - 2) / 4); // -0.25

//##########################
//Modulus - Euclidean division (Division mit Rest)

console.log(20 % 3); //2
console.log(25 % 9); //7

//##########################
//Fluff

console.log("string" / 0); //NaN (Not a Number)