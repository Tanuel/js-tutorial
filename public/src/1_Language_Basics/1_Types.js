//##########################
//Types of variables
//##########################

//How to find out the type of a variable?
// => typeof is your friend

let myVar = "value";

console.log(typeof myVar); //string

//Which types exist?

console.log(typeof 0); //number
console.log(typeof 0.0); //number
console.log(typeof Infinity); //number
console.log(typeof NaN); //number (NaN = Not a Number)

console.log(typeof "abcdefg"); //string
console.log(typeof 'abcdefg'); //string ("" and '' are basically the same)

console.log(typeof [1, 2, 3]); //object - we learn more about objects later
console.log(typeof new Array(1, 2, 3)); //object (old syntax, use [] instead)
console.log(typeof {}); //object
console.log(typeof null); //object - null represents an empty value

console.log(typeof true); //boolean
console.log(typeof false); //boolean

console.log(typeof function () {
}); //function - we learn more about functions later!

console.log(typeof undefined); //undefined (no type)
