//##########################
// Comparisons
//##########################

/* Comparisons will return a boolean value
   true =>  the comparison is correct
   false => the comparison is incorrect
 */

console.log(true);  //true
console.log(false); //false

console.log(typeof true);  //boolean
console.log(typeof false); //boolean

//##########################
//Equals comparison
//##########################

let a = 1;
let b = 2;

console.log(a == 1); //true
console.log(1 == a); //true
console.log(a == 2); //false
console.log(a == b); //false

//math operators have higher precedence
console.log(a == 1 + 1); //false => a == (1 + 1)
console.log(a == b - 1); //true => a == (b - 1)


//Typesafe comparison

console.log(1 == "1"); //true - even though 1 is a number and "1" is a string
console.log(1 === "1"); //false - because 1 is a number and "1" is a string;

//It is usually better to use === over ==

//The boolean value can be assigned:

let myBool = a === b;

console.log(myBool); //false
console.log(typeof myBool); //boolean

myBool = a === b - 1;
console.log(myBool); //true

//##########################
//Not Equals
//##########################
console.log(1 != 1); //false
console.log(1 != 2); //true

//Typesave comparison
console.log(1 !== 1);   //false
console.log(1 !== "1"); //true

//##########################
//Greater/Lighter than
//##########################
/* The operators
   >    Greater than
   >=   Greater or Equal

   <    Lighter than
   <=   Lighter or Equal
 */

console.log(1 > 1);  //false
console.log(1 >= 1); //true
console.log(1 < 1);  //false
console.log(1 <= 1); //true

console.log(1 > 2);  //false
console.log(1 >= 2); //false
console.log(1 < 2);  //true
console.log(1 <= 2); //true

//##########################
//Combining comparisons - Logical Operators
//##########################
/* Which logical operators exist?
    &&  logical AND
    ||  logical OR
    !   logical NOT
 */

a = 1;
b = 2;

// logical AND
myBool = a === 1 && b === 2; // => a equals 1 AND b equals 2
console.log(myBool); //true

myBool = a === 2 && b === 1; // => a equals 1 AND b equals 2
console.log(myBool); //false

//logical OR
console.log(a === 1 || b === 0); //true => a equals 1 OR b equals 0

//logical NOT
console.log(!true); //false
console.log(!false); //true

let c = true;

console.log(!c); //false

//combining

myBool = a === 1 && !(b === 0);
console.log(myBool); //false