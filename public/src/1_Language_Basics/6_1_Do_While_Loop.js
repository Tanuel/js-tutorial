// The Do-While Loop gets executed at least once
let i = 1;
do {
    console.log('This message got printed for ' + i++ + ' times');
} while (i <= 10);

// even if the condition is false, the block gets executed at least once
do {
    console.log('This message gets printed exactly one time');
} while (false);

//escape a loop with break;
do {
    console.log('This message gets printed infinite times... better break now!');
    break;
} while (true);