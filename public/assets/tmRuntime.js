(function (window, document) {

    let ___log = console.log;
    let ___warn = console.warn;
    let ___error = console.error;
    let ___info = console.info;

    class TmRuntime {
        constructor() {
            this._window = new TmWindow({
                title: 'TmRuntime'
            });
            this._ul = document.createElement('ul');
            this._ul.className = 'tmr-msg-list';
            this._window.appendElement(this._ul);
        }

        setTitle(title) {
            this._window.title = title;
        }

        reset() {
            while (this._ul.firstChild) {
                this._ul.removeChild(this._ul.firstChild);
            }

            this._window.title = 'TmRuntime';
            this._window.setPosition(20, 20);
            this._window.width = 600;
        }

        execute(script) {
            this._window.open();
            console.log = this.log.bind(this);
            console.warn = this.warn.bind(this);
            console.error = this.error.bind(this);
            try {
                let result = (function () {
                    return eval(script);
                })();
            } catch (e) {
                this._handleError(e);
            } finally {
                console.log = ___log;
                console.warn = ___warn;
                console.error = ___error;
                console.info = ___info;
            }
        }

        _handleError(error) {
            this._log('runtime-error', [error.stack], this._getTrace(error.stack));

            ___error(error);
        }

        _log(level, args, trace = null) {
            if(!trace){
                trace = this._getTrace();
            }
            let li = document.createElement('li');
            let textContent = '';
            let line = document.createElement('span');
            line.className = 'tmr-msg-line';
            line.textContent = 'line ' + trace.line + ': ';
            li.className = 'tmr-msg tmr-' + level;

            for (let arg of args) {
                let str = '';
                switch (typeof arg) {
                    case 'object':
                        str = JSON.stringify(arg);
                        break;
                    case 'string':
                    case 'number':
                    default:
                        str = String(arg);
                }
                textContent += str + ' ';
            }
            let tc = document.createElement('span');
            tc.className = 'tmr-msg-content';
            tc.textContent = textContent;
            li.appendChild(line);
            li.appendChild(tc);
            this._ul.appendChild(li);
        }

        log(...args) {
            this._log('log', args);
            ___log(...args)
        }

        warn(...args) {
            this._log('warn', args);
            ___warn(...args)
        }

        error(...args) {
            this._log('error', args);
            ___error(...args)
        }

        info(...args) {
            this._log('info', args);
            ___info(...args);
        }

        _getTrace(stack = null) {
            if(!stack) {
                stack = (new Error).stack;
            }
            let traceArr = stack.split('\n');
            let regex = /eval at <anonymous> (.*):(\d+):(\d+)/;

            for(let t of traceArr) {
                let match = t.match(regex);
                if (match) {
                    return {line: match[2], position: match[3]};
                }
            }
            return false;
        }
    }

    // let rt = new TmRuntime();
    // rt.execute('console.log("log");console.warn("warn");console.error("error");');

    window.TmRuntime = TmRuntime;
})(window, document);