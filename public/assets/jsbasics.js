document.addEventListener('DOMContentLoaded', (function (window, document, hljs) {
    hljs.initHighlightingOnLoad();
    let currentScript = '';
    let scriptName = '';
    let codeEl = document.getElementById('js-code');
    let runtime = new TmRuntime();
    let elements = document.querySelectorAll('[data-script]');
    console.log(elements);

    document.getElementById('btn-run-script').addEventListener('click', runScript);
    document.body.addEventListener('click', clickHandler);

    codeEl.addEventListener('blur', function(){
       hljs.highlightBlock(this);
    });

    function clickHandler(event) {
        if (event.target.dataset.hasOwnProperty('script')) {
            scriptName = event.target.dataset.script;
            loadScript(event.target.dataset.script).then(showScript);
        }
    }

    function loadScript(url) {
        return new Promise(function (resolve, reject) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4) {
                    if (this.status === 200) {
                        currentScript = this.responseText;
                        resolve(this.responseText);
                    } else {
                        reject("source not found");
                    }
                }
            };
            xhttp.open("GET", 'src/' + url, true);
            xhttp.send();
        });
    }

    function showScript(script) {
        codeEl.textContent = script;
        document.getElementById('btn-run-script').disabled = false;
        hljs.highlightBlock(codeEl);
    }

    function runScript() {
        if (document.getElementById('use-runtime').checked) {
            customRuntime();
        } else {
            try {
                let result = (function () {
                    return eval(codeEl.textContent);
                })();
            } catch (e) {
                console.log('An error occured in the executed script:');
                console.error(e);
            }
        }
    }

    function customRuntime() {
        runtime.reset();
        runtime.setTitle(scriptName);
        runtime.execute(codeEl.textContent)
    }

}).bind(null, window, document, hljs));
